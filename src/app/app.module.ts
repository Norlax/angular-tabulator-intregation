import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TabulatorModule } from './shared/tabulator-table/tabulator.module';
import { CustomSelectComponent } from './components/custom-select/custom-select.component';
import { ModalPrincipalComponent } from './components/modal-principal/modal-principal.component';
import { TablaPrincipalComponent } from './components/tabla-principal/tabla-principal.component';
import { ModalModule } from 'ngx-bootstrap/modal';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

@NgModule({
  declarations: [
    AppComponent,
    CustomSelectComponent,
    ModalPrincipalComponent,
    TablaPrincipalComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    TabulatorModule,
    ModalModule.forRoot(),
    FontAwesomeModule
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
