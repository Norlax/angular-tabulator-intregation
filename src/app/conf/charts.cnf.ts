import {colors} from './colors.cnf';
import * as moment from 'moment';

export const chart = {
    ngx: {
        singleLine: {
            results: [
                {
                    name: 'line',
                    // 50, 45, 60, 55, 70, 55, 60, 55, 65, 57, 60, 53, 53
                    series: [
                        {
                            value: 53,
                            name: '1'
                        },
                        {
                            value: 50,
                            name: '2'
                        },
                        {
                            value: 45,
                            name: '3'
                        },
                        {
                            value: 60,
                            name: '4'
                        },
                        {
                            value: 55,
                            name: '5'
                        },
                        {
                            value: 70,
                            name: '6'
                        },
                        {
                            value: 55,
                            name: '8'
                        },
                        {
                            value: 60,
                            name: '9'
                        },
                        {
                            value: 55,
                            name: '10'
                        },
                        {
                            value: 65,
                            name: '11'
                        },
                        {
                            value: 57,
                            name: '12'
                        }
                    ]
                },
            ],
            scheme: {
                customColors: [{name: 'line', value: colors.charts.singleLine}]
            },
        },
        multiLine: {
            scheme: {domain: colors.charts.multiple}
        },
        comparison: {
            scheme: {domain: colors.charts.comparison}
        },
        barVertical: {
            scheme: {domain: colors.charts.multiple}
        },
        despacho: {
          scheme: {domain: colors.charts.despacho}
        },
        formatLabels(value: string) {
            if (moment(value).isValid()) {
                return moment(value).format('MMM D');
            } else {
                return value;
            }
        }
    }
};
