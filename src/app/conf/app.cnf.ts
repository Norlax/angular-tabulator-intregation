import * as moment from 'moment';

export const app = {
    nombre: 'Logistica',
    imagen: 'null',
    grandient: 'flickler',
    initialUrl: '/auth/login',
    authenticatedUrl: '/admin/dashboards',
    datepicker: {
        min: null,
        max: new Date(),
        tz: 'America/Bogota'
    },
    daterange: {
        min: moment().subtract(15, 'days').startOf('day').toDate(),
        max: moment().startOf('day').toDate(),
        tz: 'America/Bogota'
    },
    errors: {
      required: 'Campo requerido.',
      minlength: 'Longitud inválida (${actualLength}/${requiredLength})',
      maxlength: 'Longitud inválida (${actualLength}/${requiredLength})',
      placa: 'Placa inválida.',
      identificacion: 'Identificación inválida.',
      transito: 'Vehiculo y/o conductor en tránsito',
      max: 'Peso maximo admitido es de ${max}Kg',
      min: 'Peso mínimo admitido es de ${min}Kg',
      neto: 'Peso neto debe ser mayor a cero',
      pattern: 'Solo admite numeros y letras',
  }
};
