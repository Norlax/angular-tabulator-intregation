export const colors = {
    charts: {
        comparison: [
            '#0375FF',
            '#FF6510',
        ],
        singleLine: '#FAFAFA',
        multiple: [
            '#E57373',
            '#64B5F6',
            '#7986CB',
            '#81C784',
            '#FFF176',
            '#FFB74D',
        ],
        despacho: [
          '#2F8BE6'
        ]
    }
};
