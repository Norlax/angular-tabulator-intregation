import * as moment from 'moment-timezone';


export function formatDate(v, f = 'YYYY-MM-DD') {
    return Boolean(v) ? moment(v).tz('America/Bogota').locale('es').format(f) : v;
}

export function stringToDate(fecha: string | null, format = null, language = 'es'): Date | null {
    return Boolean(fecha) ? moment(fecha, format, language, true).tz('America/Bogota').toDate() : null;
}

export function formatDateHour2(v, f = 'YYYY/MM/DD h:mm A') {
    return moment(v).subtract(5, 'hours').format(f);
}

export function strToDateStrict(fecha: string | null): Date | null {
    if (Boolean(fecha) && moment(fecha).isValid()) {
        const dateArr = fecha.split('T');
        const dateStract = dateArr[0].split('-').map(Number);
        const timeStract = dateArr[1].split('.')[0].split(':').map(Number);
        return new Date(dateStract[0], dateStract[1] - 1, dateStract[2], timeStract[0], timeStract[1], timeStract[2]);
    }
    return null;
}

export function strToDateStrict2(fecha: string | null): Date | null {
    if (Boolean(fecha) && moment(fecha).isValid()) {
        const dateArr = fecha;
        const dateStract = dateArr[0].split('-').map(Number);
        const timeStract = dateArr[1].split('.')[0].split(':').map(Number);
        return new Date(dateStract[0], dateStract[1] - 1, dateStract[2], timeStract[0], timeStract[1], timeStract[2]);
    }
    return null;
}

export function formatSimpleFecha(v, f = 'YYYY-MM-DD') {
  return moment(v).format(f);
}
