import * as _ from 'lodash';

export function formatColumns(data_columnas, data_res) {
  // console.log('llegue')
  let formatConfigs = data_res;
  let copiaColumn = _.cloneDeep(data_columnas);
  let formatoColumn = [];

  formatConfigs.forEach((i, idx) => {
    let encontreColumn = copiaColumn.find((z) => _.trim(z.title) === _.trim(i.title));
    if (Boolean(encontreColumn) === true) {
      encontreColumn.visible = i.visible;
      encontreColumn.headerFilter = i.headerFilter;
      encontreColumn.frozen = i.frozen;
      formatoColumn.push(encontreColumn);
    }
  });

  // console.log('Reenvio', formatoColumn);

  return formatoColumn
}
