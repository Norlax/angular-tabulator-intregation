import * as _ from 'lodash';
import {FormArray, FormControl, FormGroup} from '@angular/forms';

export function objectValuesToUppercase(form: Object) {
    for (const k in form) {
        if (form.hasOwnProperty(k)) {
            if (_.isString(form[k])) {
                form[k] = form[k].toUpperCase();
            }
        }
    }
    return form;
}


export function formErrors(form: FormGroup) {
    Object.keys(form.controls)
        .forEach(i => {
            if (form.controls[i].invalid) {
                if (form.controls[i] instanceof FormControl) {
                    console.log(i, form.controls[i].errors);
                }
                if (form.controls[i] instanceof FormGroup) {
                    formErrors(form.controls[i] as FormGroup);
                }
            }
        });
}


export function validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
        const control = formGroup.get(field);
        if (control instanceof FormControl) {
            control.markAsTouched({onlySelf: true});
        } else if (control instanceof FormGroup) {
            validateAllFormFields(control);
        } else if (control instanceof FormArray) {
            for (let i = 0; i <= control.length; i++) {
                if (Boolean(control.controls[i])) {
                    validateAllFormFields(control.at(i) as FormGroup);
                }
            }
        }
    });
}

export function markAsTouched(formGroup: FormGroup) {
  // @ts-ignore
  Object.keys(formGroup.controls).forEach(field => {
    const control = formGroup.get(field);
    if (control instanceof FormControl) {
      control.markAsTouched({onlySelf: true});
    } else if (control instanceof FormGroup) {
      markAsTouched(control);
    } else if (control instanceof FormArray) {
      for (let i = 0; i <= control.length; i++) {
        if (Boolean(control.controls[i])) {
          markAsTouched(control.at(i) as FormGroup);
        }
      }
    }
  });
}
