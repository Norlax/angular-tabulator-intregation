import * as moment from 'moment';

export function formatDate(v, f = 'YYYY-MM-DD') {
  return moment(v).format(f);
}

export function formatDateHour(v, f = 'YYYY-MM-DD, h:mm') {
  return moment(v).format(f);
}

export function formatoMoment(v, f = 'YYYY-MM-DD, h:mm a') {
  return moment(v).format(f);
}

export function formatDateTime(v, f = 'YYYY-MM-DD hh:mm:ss') {
  return Boolean(v) ? moment(v).tz('America/Bogota').locale('es').format(f) : v;
}

export function formatDateCustom(v, f = 'YYYY-MM-DD, hh:mm') {
  return moment(v).format(f);
}
