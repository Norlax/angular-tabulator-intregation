import { TableComponent } from "../shared/tabulator-table/components/table/table.component";


export function setNombreArchivo(nombre: string, id: string) {
  TableComponent.nombreArchivo.nombreArchivo = nombre;
  TableComponent.nombreArchivo.tableid = id;
  return true
}
