import * as moment from 'moment';
import * as _ from 'lodash';

export function filtrarRangoFecha(datos, tipo, fechaIniComp, fechaFinComp, origen, origen_flag, destino, destino_flag, transport, transport_flag) {
  let dataFiltrada = [];
  let dataFinal = [];

  /* con este switch se filtra o no la data que llega a la funcion */
  // sin load: estamos utilizando la data dentro del rango de fechas ya establecido por ende hay que filtrar
  // con load: estamos utilizando la data fuera del rango de fechas ua viene filtrada
  switch(tipo) {
    case 'sinload':
      dataFiltrada = datos.filter((i) => moment(i.fecha_despacho).format('YYYY-MM-DD') >= moment(fechaIniComp).format('YYYY-MM-DD') && moment(i.fecha_despacho).format('YYYY-MM-DD') <= moment(fechaFinComp).format('YYYY-MM-DD'));
      break;
    case 'conload':
      dataFiltrada = _.cloneDeep(datos);
      break;
  }

  /* En cada if se pregunta que filtros estan activos */
  //Si cualquiera de los filtros esta activo se filtrara la data en orden de filtro
  if (origen_flag === true) {
    dataFiltrada = dataFiltrada.filter((i) => i.origen === origen);
  }

  if (destino_flag === true) {
    dataFiltrada = dataFiltrada.filter((i) => i.destino === destino);
  }

  if (transport_flag === true) {
    dataFiltrada = dataFiltrada.filter((i) => i.empresa_despachar === transport);
  }

  return dataFiltrada
}

export function filtrarRangoFechaFact(datos, tipo, fechaIniComp, fechaFinComp, transport, transport_flag) {
  let dataFiltrada = [];
  let dataFinal = [];

  switch(tipo) {
    case 'sinload':
      dataFiltrada = datos.filter((i) => moment(i.fecha_despacho).format('YYYY-MM-DD') >= moment(fechaIniComp).format('YYYY-MM-DD') && moment(i.fecha_despacho).format('YYYY-MM-DD') <= moment(fechaFinComp).format('YYYY-MM-DD'));
      break;
    case 'conload':
      dataFiltrada = _.cloneDeep(datos);
      break;
  }

  if (transport_flag === true) {
    dataFiltrada = dataFiltrada.filter((i) => i.Nombre === transport);
  }

  return dataFiltrada
}

export function filtrarRangoCobros(datos, tipo, fechaIniComp, fechaFinComp, ruta, ruta_flag, transport, transport_flag) {
  let dataFiltrada = [];
  let dataFinal = [];

  switch(tipo) {
    case 'sinload':
      dataFiltrada = datos.filter((i) => moment(i.fecha_carga).format('YYYY-MM-DD') >= moment(fechaIniComp).format('YYYY-MM-DD') && moment(i.fecha_carga).format('YYYY-MM-DD') <= moment(fechaFinComp).format('YYYY-MM-DD'));
      break;
    case 'conload':
      dataFiltrada = _.cloneDeep(datos);
      break;
  }

  if (ruta_flag === true) {
    dataFiltrada = dataFiltrada.filter((i) => i.ruta === ruta);
  }

  if (transport_flag === true) {
    dataFiltrada = dataFiltrada.filter((i) => i.empresa_despachar === transport);
  }

  return dataFiltrada
}

export function filtrarRangoInformesIndividual(datos, tipo, fechaIniComp, fechaFinComp, transport, transport_flag, origen, origen_flag, destino, destino_flag, producto, producto_flag) {
  let dataFiltrada = [];
  let dataFinal = [];

  // console.log(datos, tipo, fechaIniComp, fechaFinComp, transport, transport_flag, origen, origen_flag, destino, destino_flag, producto, producto_flag)

  switch(tipo) {
    case 'sinload':
      dataFiltrada = _.cloneDeep(datos);
      // dataFiltrada = datos.filter((i) => moment(i.fecha_filtrar).format('YYYY-MM-DD') >= moment(fechaIniComp).format('YYYY-MM-DD') && moment(i.fecha_filtrar).format('YYYY-MM-DD') <= moment(fechaFinComp).format('YYYY-MM-DD'));
      // console.log('Fechas', moment(fechaIniComp).format('YYYY-MM-DD'))
      break;
    case 'conload':
      dataFiltrada = _.cloneDeep(datos);
      break;
  }

  // console.log('filtro', dataFiltrada);

  if (transport_flag === true) {
    dataFiltrada = dataFiltrada.filter((i) => i.transp_filtrar === transport);
  }
  if (origen_flag === true) {
    dataFiltrada = dataFiltrada.filter((i) => i.origen_filtrar === origen);
  }
  if (destino_flag === true) {
    dataFiltrada = dataFiltrada.filter((i) => i.destino_filtrar === destino);
  }
  if (producto_flag === true) {
    dataFiltrada = dataFiltrada.filter((i) => i.producto_filtrar === producto);
  }

  // console.log('filtro', dataFiltrada);

  return dataFiltrada
}

export function filtrarRangoInformes(datos, tipo, fechaIniComp, fechaFinComp, transport, transport_flag, origen, origen_flag, destino, destino_flag, producto, producto_flag) {
  let dataFiltrada = [];
  let dataFinal = [];

  // console.log(datos, tipo, fechaIniComp, fechaFinComp, transport, transport_flag, origen, origen_flag, destino, destino_flag, producto, producto_flag)

  switch(tipo) {
    case 'sinload':
      dataFiltrada = datos.filter((i) => moment(i.fecha_filtrar).format('YYYY-MM-DD') >= moment(fechaIniComp).format('YYYY-MM-DD') && moment(i.fecha_filtrar).format('YYYY-MM-DD') <= moment(fechaFinComp).format('YYYY-MM-DD'));
      // console.log('Fechas', moment(fechaIniComp).format('YYYY-MM-DD'))
      break;
    case 'conload':
      dataFiltrada = _.cloneDeep(datos);
      break;
  }

  // console.log('filtro', dataFiltrada);

  if (transport_flag === true) {
    dataFiltrada = dataFiltrada.filter((i) => i.transp_filtrar === transport);
  }
  if (origen_flag === true) {
    dataFiltrada = dataFiltrada.filter((i) => i.origen_filtrar === origen);
  }
  if (destino_flag === true) {
    dataFiltrada = dataFiltrada.filter((i) => i.destino_filtrar === destino);
  }
  if (producto_flag === true) {
    dataFiltrada = dataFiltrada.filter((i) => i.producto_filtrar === producto);
  }

  // console.log('filtro', dataFiltrada);

  return dataFiltrada
}

export function filtrarRangoInformesB(datos, tipo, fechaIniComp, fechaFinComp, transport, transport_flag, origen, origen_flag, destino, destino_flag, producto, producto_flag) {
  let dataFiltrada = [];
  let dataFinal = [];

  // console.log(datos, tipo, fechaIniComp, fechaFinComp, transport, transport_flag, origen, origen_flag, destino, destino_flag, producto, producto_flag)

  switch(tipo) {
    case 'sinload':
      dataFiltrada = datos.filter((i) => moment(i.fecha_filtrar).format('YYYY-MM-DD') >= moment(fechaIniComp).format('YYYY-MM-DD') && moment(i.fecha_filtrar).format('YYYY-MM-DD') <= moment(fechaFinComp).format('YYYY-MM-DD'));
      break;
    case 'conload':
      dataFiltrada = _.cloneDeep(datos);
      break;
  }

  if (transport_flag === true) {
    dataFiltrada = dataFiltrada.filter((i) => i.transp_filtrar === transport);
  }
  if (origen_flag === true) {
    dataFiltrada = dataFiltrada.filter((i) => i.origen_filtrar === origen);
  }
  if (destino_flag === true) {
    dataFiltrada = dataFiltrada.filter((i) => i.destino_filtrar === destino);
  }
  if (producto_flag === true) {
    dataFiltrada = dataFiltrada.filter((i) => i.producto_filtrar === producto);
  }

  return dataFiltrada
}

export function filtroUniCampo(datos, valor, valor_flag) {
  let dataFiltrada = [];
  let dataFinal = [];

  dataFiltrada = datos;

  if (valor_flag === true) {
    dataFiltrada = dataFiltrada.filter((i) => i.Nombre === valor);
  }

  return dataFiltrada
}

export function filtroDespacho(datos, tipo, fechaIniComp, fechaFinComp, origen_select, origen_flag, destino_select, destino_flag, producto_select, producto_flag) {
  let dataFiltrada = datos;

  // console.log('revision', fechaIniComp, fechaFinComp, origen_select, origen_flag, destino_select, destino_flag, producto_select, producto_flag)

  switch(tipo) {
    case 'sinload':
      dataFiltrada = datos.filter((i) => moment(i.fecha_filtrar).format('YYYY-MM-DD') >= moment(fechaIniComp).format('YYYY-MM-DD') && moment(i.fecha_filtrar).format('YYYY-MM-DD') <= moment(fechaFinComp).format('YYYY-MM-DD'));
      break;
    case 'conload':
      dataFiltrada = _.cloneDeep(datos);
      break;
  }

  if (origen_flag === true) {
    dataFiltrada = dataFiltrada.filter((i) => i.origen_filtrar === origen_select);
  }
  if (destino_flag === true) {
    dataFiltrada = dataFiltrada.filter((i) => i.destino_filtrar === destino_select);
  }
  if (producto_flag === true) {
    dataFiltrada = dataFiltrada.filter((i) => i.producto_filtrar === producto_select);
  }

  return dataFiltrada
}
