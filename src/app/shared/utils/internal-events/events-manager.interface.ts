import {Subject} from 'rxjs';

export interface IEventsManager {
  [key: string]: Subject<any>;
}
