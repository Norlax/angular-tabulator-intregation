export type ColumnObjectType<T> = {
    [P in keyof T | string]?: CustomColumnDefiniction<T>;
};

export type CustomColumnDefiniction<T> = Tabulator.ColumnDefinition & { field: keyof T };
