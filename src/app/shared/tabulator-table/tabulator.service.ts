import {Injectable} from '@angular/core';
import {Observable, Subject} from 'rxjs';

export interface ConfigEnabled {
    action: 'visible' | 'headerSort' | 'headerFilter' | 'frozen' | 'extraer'
    | 'seteo' | 'data' | 'recalcular'
    | 'paginacion' | 'clear-Filtros';
    field: string;
}

export interface TabConfig {
    [key: string]: Subject<ConfigEnabled>;
}

export interface ActionEvents {
    [key: string]: { [key: string]: Subject<any> };
}

@Injectable({
    providedIn: 'root'
})
export class TabulatorService {
    private config$: TabConfig = {};
    private actions$: ActionEvents = {};

    constructor() {
    }

    private createBehaviorSubjectConfigIfNotExists(id) {
        if (!this.config$.hasOwnProperty(id)) {
            this.config$[id] = new Subject();
        }
    }

    private createSubjectActionIfNotExists(id, action) {
        if (!this.actions$.hasOwnProperty(id)) {
            this.actions$[id] = {};
        }
        if (!this.actions$[id].hasOwnProperty(action)) {
            this.actions$[id][action] = new Subject();
        }
    }

    publish(id: string, defaultValue?: any): void {
        // console.log(id, defaultValue)
        this.createBehaviorSubjectConfigIfNotExists(id);
        this.config$[id].next(defaultValue);
    }

    triggerAction(id: string, action: string, defaultValue?: any) {
        this.createSubjectActionIfNotExists(id, action);
        this.actions$[id][action].next(defaultValue);
    }

    onAction(id: string, action: string) {
        this.createSubjectActionIfNotExists(id, action);
        return this.actions$[id][action].asObservable();
    }

    listen(id: string): Observable<ConfigEnabled> {
        this.createBehaviorSubjectConfigIfNotExists(id);
        return this.config$[id].asObservable();
    }
}
