import {Component, OnDestroy} from '@angular/core';
import {Subscription} from 'rxjs';

@Component({
  template: '',
})
export abstract class CoreComponent implements OnDestroy {
  protected _subscription = new Subscription();

  get subscription(): Subscription {
    return this._subscription;
  }

  set subscription(value: Subscription) {
    this._subscription.add(value);
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
