import {OnDestroy, OnInit} from '@angular/core';
import {Subject} from 'rxjs';
import {CoreComponent} from './core.component';

export abstract class CoreTabulatorComponent extends CoreComponent implements OnInit, OnDestroy {
    abstract tableId: string;
    abstract settigns: Tabulator.Options;
    abstract columns?: Tabulator.ColumnDefinition[];
    public data: any[] = [];
    protected dateRange$ = new Subject<Date[]>();

    ngOnInit(): void {
        this.loadData();
    }

    abstract loadData(...args): void;

    setDateRange(range: Date[]) {
        this.dateRange$.next(range);
    }
}
