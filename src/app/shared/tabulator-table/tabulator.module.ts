import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TableComponent} from './components/table/table.component';
import {TabulatorComponent} from './components/tabulator/tabulator.component';
import {TabulatorConfigComponent} from './components/tabulator-config/tabulator-config.component';
import {TabulatorTriggerDirective} from './directives/tabulator-trigger.directive';
import {ModalModule} from 'ngx-bootstrap/modal';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {FilterFieldsPipe} from './pipes/filter-fields.pipe';
import {DragDropModule} from '@angular/cdk/drag-drop';
import {TabulatorDownloadDirective} from './directives/tabulator-download.directive';
import {TabulatorDatepickerComponent} from './components/tabulator-datepicker/tabulator-datepicker.component';
import {TabulatorTagListComponent} from './components/tabulator-tag-list/tabulator-tag-list.component';
import {TabulatorTagButtonComponent} from './components/tabulator-tag-button/tabulator-tag-button.component';
import {TabulatorFiltroComponent} from './components/tabulator-filtro/tabulator-filtro.component';
import {BsDatepickerModule} from "ngx-bootstrap/datepicker";
import {TooltipModule} from "@swimlane/ngx-charts";
import { TabulatorSaveconfigComponent } from './components/tabulator-saveconfig/tabulator-saveconfig.component';
import { TabulatorConfigConfirmComponent } from './components/tabulator-config-confirm/tabulator-config-confirm.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';


@NgModule({
  declarations: [
    TableComponent,
    TabulatorComponent,
    TabulatorConfigComponent,
    TabulatorTriggerDirective,
    FilterFieldsPipe,
    TabulatorDownloadDirective,
    TabulatorDatepickerComponent,
    TabulatorTagListComponent,
    TabulatorTagButtonComponent,
    TabulatorFiltroComponent,
    TabulatorSaveconfigComponent,
    TabulatorConfigConfirmComponent,
  ],
  exports: [TabulatorComponent, TableComponent, TabulatorFiltroComponent],
  entryComponents: [TabulatorConfigComponent],
  imports: [
    CommonModule,
    ModalModule.forRoot(),
    FormsModule,
    ReactiveFormsModule,
    DragDropModule,
    BsDatepickerModule.forRoot(),
    TooltipModule,
    NgbModule,
    FontAwesomeModule,
  ],
})
export class TabulatorModule {}
