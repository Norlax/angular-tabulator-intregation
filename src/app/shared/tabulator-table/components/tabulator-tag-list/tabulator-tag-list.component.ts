import {Component, Input, OnInit, Output} from '@angular/core';
import {CdkDragDrop, copyArrayItem, moveItemInArray, transferArrayItem} from '@angular/cdk/drag-drop';
import {animate, animateChild, query, stagger, style, transition, trigger} from '@angular/animations';

@Component({
    selector: 'app-tabulator-tag-list',
    templateUrl: './tabulator-tag-list.component.html',
    styleUrls: ['./tabulator-tag-list.component.scss'],
    animations: [
        trigger('items', [
            transition(':enter', [
                style({transform: 'scale(0.5)', opacity: 0}),  // initial
                animate(
                    '300ms cubic-bezier(.8, -0.6, 0.2, 1.5)',
                    style({transform: 'scale(1)', opacity: 1}),
                )  // final
            ]),
            transition(':leave', [
                style({transform: 'scale(1)', opacity: 1, height: '*'}),
                animate('1s cubic-bezier(.8, -0.6, 0.2, 1.5)',
                    style({
                        transform: 'scale(0.5)', opacity: 0,
                        height: '0px', margin: '0px'
                    }))
            ])
        ]),
        trigger('list', [
            transition(':enter', [
                query('@items', stagger(300, animateChild()), {optional: true})
            ]),
        ])
    ]
})
export class TabulatorTagListComponent implements OnInit {
    @Input()
    @Output()
    tagList: any[] = [];

    @Input()
    copyItem = false;

    constructor() {
    }

    ngOnInit() {
    }

    drop($event: CdkDragDrop<any[]>) {
        if ($event.container !== $event.previousContainer) {
            if (this.copyItem) {
                copyArrayItem($event.previousContainer.data, $event.container.data, $event.previousIndex, $event.currentIndex);
            } else {
                transferArrayItem(
                    $event.previousContainer.data,
                    $event.container.data,
                    $event.previousIndex,
                    $event.currentIndex);
            }
        } else {
            moveItemInArray($event.container.data || this.tagList, $event.previousIndex, $event.currentIndex);
        }
    }

    removeAt(idx: any) {
        this.tagList.splice(idx, 1);
    }

    removeAll() {
        this.tagList.splice(0, this.tagList.length);
    }
}
