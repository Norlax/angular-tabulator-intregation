import { Component, EventEmitter, Input, OnDestroy, Output, ViewChild, ChangeDetectorRef } from '@angular/core';
import {defaultColumns, defaultParams} from '../../tools/default-params';
import {Subscription} from 'rxjs';
import {BsModalService} from 'ngx-bootstrap/modal';
import {TabulatorService} from '../../tabulator.service';
import {TabulatorSaveconfigComponent} from '../tabulator-saveconfig/tabulator-saveconfig.component';
import {TabulatorConfigConfirmComponent} from '../tabulator-config-confirm/tabulator-config-confirm.component';
import {TableComponent} from '../table/table.component';
import {cloneDeep} from 'lodash';
import { map } from 'rxjs/operators';
import { defaultParamsResponsive } from '../../tools/default-params-responsive';
import { EventsManagerService } from 'src/app/shared/utils/internal-events/events-manager.service';
import { TablaConfigService } from 'src/app/shared/services/tabla-config.service';
import { faCoffee, faCloudDownloadAlt, faTools } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-tabulator',
  templateUrl: './tabulator.component.html',
  styleUrls: ['./tabulator.component.scss'],
})
export class TabulatorComponent implements OnDestroy {
  private _settigns: Tabulator.Options = defaultParams;
  // private _settigns: Tabulator.Options = defaultParamsResponsive;
  public usuario: any = JSON.parse(window.localStorage.getItem('user'));
  @ViewChild(TableComponent) table: TableComponent;
  // Variables para la funcion de guardar configuracion
  data_config: any;
  config_seleccionado: any;
  entidad_actual: any;
  flag_seleccionado = false;
  isCollapsed = false;
  select_config: any = null;
  faCoffee = faCoffee;
  faCloudDownloadAlt = faCloudDownloadAlt;
  faTools = faTools;

  @Output()
  dateRangePickerChange = new EventEmitter();

  @Input()
  datepicker = true;

  @Input() campo;
  @Input() entidad;
  @Input() botonCrear;
  @Input() botonMensaje;
  @Input() botonUsuario;
  @Input() botonPremio;
  @Input() entidadTabla;
  @Input() estiloMensaje;
  @Input() botonConfig;
  @Input() descargar = true;
  @Input() tabulator_nuevo = true;
  @Input() flag_shadow;
  @Input() flag_semishadow;
  @Input() flag_pseudoshadow;
  @Input() flag_size;
  @Input() temporal;
  @Input() flag_gray;
  @Input() flag_factura;
  @Input() flagDescarga;
  @Input() flagResponsive;

  private _subscription = new Subscription();
  get subscription(): Subscription {
    return this._subscription;
  }

  set subscription(value: Subscription) {
    this._subscription.add(value);
  }

  @Input() set settigns(value: Tabulator.Options) {
    this._settigns = Object.assign(cloneDeep(this._settigns), value);
  }

  get settigns(): Tabulator.Options {
    return this._settigns;
  }

  @Input() data: { [key: string]: any }[] = [];
  @Input() tableId = 'general-table';

  @Input() set columns(value: Tabulator.ColumnDefinition[]) {
    this._columns = defaultColumns(this._settigns, value);
  }

  private _columns: Tabulator.ColumnDefinition[] = [];

  get columns(): Tabulator.ColumnDefinition[] {
    if (this.flagResponsive === true && this._columns[0].field === '#') {
      this._columns.splice(0, 1);
      this.flagResponsive = false;

      return this._columns;
    } else {
      return this._columns;
    }
  }

  constructor(
    public events: EventsManagerService,
    // public toas: ToastrService,
    private modalService: BsModalService,
    private readonly tabulatorService: TabulatorService,
    private configSer: TablaConfigService,
    private cd: ChangeDetectorRef
  ) {
    this.subscription = this.events
      .subscribe('guardar-config')
      .subscribe(() => this.loadConfig());
    this.subscription = this.events
      .subscribe('entidad-tabulator')
      .subscribe((data) => (this.entidad_actual = data));
    this.subscription = this.events
      .subscribe('reload-configs')
      .subscribe(() => {
        this.data_config = [];
        this.loadConfig();
        this.flag_seleccionado = false;
        this.cd.detectChanges();
      });
    // this.loadConfig();
  }

  loadConfig() {
    let datos_total;
    this.configSer
      .listarConfig()
      /* .pipe(
      map((i) => {
        console.log('la i', i)
        return i;
      })
    ) */
      .subscribe(
        (data_res) => {
          if (data_res) {
            if (data_res.length > 0) {
              for (let i of data_res) {
                if (i.tabla.toUpperCase() === this.tableId.toUpperCase()) {
                  datos_total = data_res;
                }
              }
            } else {
              datos_total = [];
            }
          } else {
            datos_total = [];
          }
        },
        (e) => {},
        () => {
          if (datos_total) {
            if (datos_total.length > 0) {
              this.data_config = datos_total.filter(
                (i) => i.tabla.toUpperCase() === this.tableId.toUpperCase()
              );
              let configFiltrada = this.data_config.find(
                (i) => i.predeterminado === 'SI'
              );

              if (Boolean(configFiltrada) === true) {
                setTimeout(() => {
                  this.select_config = configFiltrada.id;
                  this.flag_seleccionado = true;
                  this.config_seleccionado = [configFiltrada];
                  this.events.publish('seleccion-config', [configFiltrada]);
                }, 1000);
              }
            }

            this.cd.detectChanges();
          }
        }
      );
  }

  crearConfig(tipo) {
    // console.log('Configuracion seleccionada', this.config_seleccionado)
    this.modalService.show(TabulatorSaveconfigComponent, {
      animated: true,
      class: 'modal-md',
      initialState: {
        tableId: this.entidadTabla,
        seccion: tipo,
        config_actual: this.config_seleccionado,
      },
    });

    this.tabulatorService.publish(this.entidadTabla, {
      action: 'extraer',
    });
  }

  selectConfig(event) {
    const data_select = this.data_config.filter(
      (i) =>
        i.id === this.select_config && i.id_usuario === this.usuario.usuarioId
    );
    this.config_seleccionado = data_select;
    this.flag_seleccionado = true;
    this.events.publish('seleccion-config', data_select);
  }

  eliiminarConfig() {
    this.modalService.show(TabulatorConfigConfirmComponent, {
      animated: true,
      class: 'modal-sm',
      initialState: { datos: this.config_seleccionado[0] },
    });
  }

  ngOnDestroy(): void {
    this.settigns = Object.assign({}, {});
    this.subscription.unsubscribe();
  }

  crearLink(entidad): void {}

  refresh(entidad) {
    // console.log(entidad);
  }
}
