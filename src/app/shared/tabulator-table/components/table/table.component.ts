import {
    AfterViewInit,
  ChangeDetectorRef,
    Component,
    ElementRef,
    HostListener,
    Input,
    NgZone,
    OnChanges,
    OnDestroy,
    SimpleChanges,
    ViewChild,
    ViewEncapsulation
} from '@angular/core';
import {TabulatorService} from '../../tabulator.service';
import {Subject, Subscription} from 'rxjs';
import {debounceTime} from 'rxjs/operators';
import {cloneDeep} from 'lodash';
import * as moment from 'moment';
import { EventsManagerService } from 'src/app/shared/utils/internal-events/events-manager.service';
import { TablaPrincipalComponent } from 'src/app/components/tabla-principal/tabla-principal.component';

@Component({
    selector: 'app-table',
    template: '<div #table></div>',
    styleUrls: ['./table.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class TableComponent implements OnChanges, AfterViewInit, OnDestroy {
  static data_config: any;
  static data_editar: any;
  static nombreArchivo = {
    nombreArchivo: 'default',
    tableid: ''
  };
  @Input() settigns: Tabulator.Options = {};
  @Input() data;
  @Input() columns;
  @Input() tableId = 'global-table';
  @ViewChild('table') tableEl: ElementRef<HTMLDivElement>;
  table: Tabulator;
  redraw = new Subject<boolean>();
  private _subscription = new Subscription();
  get subscription(): Subscription {
      return this._subscription;
  }

  set subscription(value: Subscription) {
      this._subscription.add(value);
  }

  constructor(private readonly ngZone: NgZone,
              private event: EventsManagerService,
              private cd: ChangeDetectorRef,
              private readonly tableService: TabulatorService) {
  }

  private drawTable(): void {
      this.ngZone.runOutsideAngular(() => {
          if (this.columns) {
              this.settigns.columns = [...this.columns];
          }
          if (this.data) {
              this.settigns.data = this.cloneDeep();
          }
          let int = setInterval(() => {
              if (this.tableEl && this.tableEl.nativeElement) {
                  this.table = new Tabulator(this.tableEl.nativeElement, this.settigns);
                  clearInterval(int)
              }
          }, 1000)
      });
  }

  ngAfterViewInit(): void {
      this.listen();
      this.drawTable();
      this.subscription = this.event.subscribe('add-row').subscribe((item) => this.addRow(item));
      this.subscription = this.event.subscribe('delete-row').subscribe(() => this.removerRow());
      this.subscription = this.event.subscribe('edit-row').subscribe((itemRow) => this.editCampos(itemRow));
      this.subscription = this.event.subscribe('redibujo').subscribe(() => {
        console.log('llegue?')
        this.redibujo();
      });
  }

  ngOnDestroy(): void {
      this.subscription.unsubscribe();
  }

  listen() {
      this.subscription = this.tableService.listen(this.tableId)
          .subscribe((changes) => {
              let columns;
              switch (changes.action) {
                  case 'visible':
                      this.table.toggleColumn(changes.field);
                      columns = this.table.getColumnDefinitions().map(c => {
                          if (c.field === changes.field) {
                              c.visible = c.hasOwnProperty('visible') ? !c.visible : false;
                          }
                          return c;
                      });
                      break;
                  case 'headerSort':
                      columns = this.table.getColumnDefinitions().map(c => {
                          if (c.field === changes.field) {
                              c.headerSort = c.hasOwnProperty('headerSort') ? !c.headerSort : false;
                          }
                          return c;
                      });
                      this.table.setColumns(columns);
                      break;
                  case 'headerFilter':
                      columns = this.table.getColumnDefinitions().map(c => {
                          if (c.field === changes.field) {
                              c.headerFilter = c.hasOwnProperty('headerFilter') ? !c.headerFilter : true;
                          }
                          return c;
                      });
                      this.table.setColumns(columns);
                      break;
                  case 'frozen':
                      columns = this.table.getColumnDefinitions().map(c => {
                          const u = cloneDeep(c);
                          if (u.field === changes.field) {
                              u.frozen = !Boolean(u.frozen);
                          }
                          return u;
                      });
                      this.table.setColumns(columns);
                      break;
                  case 'extraer':
                  /* columns = this.table.getColumnDefinitions().map(c => {
                    console.log('el recorrido de la columna', c);
                    const u = cloneDeep(c);
                    if (u.field === changes.field) {
                      u.frozen = !Boolean(u.frozen);
                    }
                    return u;
                  }); */
                  columns = this.table.getColumnDefinitions();
                  TableComponent.data_config = columns;
                  break;
                  case 'seteo':
                  this.table.setColumnLayout(this.columns);
                  break;
                  case 'data':
                    let filteredData= this.table.getData(true);
                    TablaPrincipalComponent.dataFiltrada = filteredData;
                    break;
                  case 'recalcular':
                    console.log('Calculo?')
                    // * Evento recalcular
                    // ! se activa la funcion recalc del tabulator, la cual vuelve a recalcular las sumatorias de las columnas
                    this.table.recalc();
                    break;
                  case 'paginacion':
                    if (this.table.getPage() !== 1) {
                      this.table.setPage(1);
                    }
                    break;
                  case 'clear-Filtros':
                    this.table.clearSort();
                    this.table.clearHeaderFilter();
                    break;
              }
          });

      this.subscription = this.tableService.onAction(this.tableId, 'download')
          .subscribe((options: Tabulator.DownloadParams) => {
            /* console.log('Se ejecuto?', this.tableId)
            console.log('Se ejecuto? x2', TableComponent.nombreArchivo) */
            if (TableComponent.nombreArchivo.tableid === this.tableId) {
              this.download(options.downloadType, `${TableComponent.nombreArchivo.nombreArchivo}_${moment().format('DD-MM-YYYY hh:mm:ss a')}.xlsx`);
            }
          });
      this.redraw.pipe(debounceTime(500))
          .subscribe((flag: boolean) => {
            setTimeout(() => {
              this.table.redraw(true)
            }, 500);
          });
  }

  async ngOnChanges(changes: SimpleChanges): Promise<void> {
      if (changes.settigns) {
          this.drawTable();
      }
      if (changes.data) {
          let int = setInterval(async () => {
              if (this.table) {
                  await this.table.setData(this.cloneDeep());
                  clearInterval(int)
              }
          }, 1000)
      }
  }

  download(type: Tabulator.DownloadType, filename: string) {
      // console.log(type, filename);
      // this.toast.success('Exportado con exito', 'Descargando')
      //this.table.download(type, filename);
      this.table.download('xlsx', filename); // download a xlsx file that has a sheet name of "MyData"
  }

  @HostListener('window:resize')
  onResize() {
      this.redraw.next(true);
  }

  cloneDeep() {
      // Se realiza este proceso para eliminar referencias de memoria y proteger la escritura de datos en la fuente de datos,
      // protegiendo la inmutabilidad de los datos
      // In case of memory LEAK use => JSON.parse(JSON.stringify(this.data));
      return cloneDeep(this.data);
  }

  redibujo() {
    console.log('??')
    // this.redraw.next(true);
    this.table.redraw(true)
  }

  addRow(item) {
    // console.log('addrow', item);
    if (item === true) {
      this.table.addRow({
        origen: '',
        patio: '',
        destino: '',
        puerto: '',
        precio: '',
        bono: '',
        transportadora: [],
        seleccionado: 1
      }, true);
    } else {
      let row = this.table.rowManager.rows[0];
      // console.log('crear', row)
      this.table.deleteRow(row);
      setTimeout(() => {
        this.table.addRow({
          origen: item.origen,
          patio: item.patio || 'No Aplica',
          destino: item.destino || 'No Aplica',
          puerto: item.puerto || 'No Aplica',
          precio: item.flete,
          bono: item.bono || 'N/A',
          transportadora: item.transportadoras,
          seleccionado: 2
        }, true);
      }, 500)
    }
  }

  editCampos(itemRow) {
    let fila_posicion = this.table.getRowPosition(itemRow, true);
    let row = this.table.rowManager.rows[fila_posicion]
    // console.log('La fila', row);
    if (Boolean(row) === true) {
      this.table.deleteRow(row);
      this.table.addRow({
        origen: TableComponent.data_editar.origen,
        patio: TableComponent.data_editar.patio || 'No Aplica',
        destino: TableComponent.data_editar.destino || 'No Aplica',
        puerto: TableComponent.data_editar.puerto || 'No Aplica',
        precio: '',
        bono: '',
        transportadora: [],
        seleccionado: 1
      }, true)
      // console.log('ooo', TableComponent.data_editar)
      this.event.publish('editar-precio', TableComponent.data_editar.precio);
      this.event.publish('editar-bono', TableComponent.data_editar.bono);
    }
    /* setTimeout(() => {
      this.table.updateRow(row, {
        origen: TableComponent.data_editar.origen,
        patio: TableComponent.data_editar.patio || 'No Aplica',
        destino: TableComponent.data_editar.destino || 'No Aplica',
        puerto: TableComponent.data_editar.puerto || 'No Aplica',
        precio: 0,
        bono: 0,
        transportadora: []
      });
    }, 500) */

    /* this.table.updateOrAddRow(row, {
      origen: TableComponent.data_editar.origen,
      patio: TableComponent.data_editar.patio || 'No Aplica',
      destino: TableComponent.data_editar.destino || 'No Aplica',
      puerto: TableComponent.data_editar.puerto || 'No Aplica',
      precio: 0,
      bono: 0,
      transportadora: []
    }); */
  }

  removerRow() {
    // let row = this.table.getRow(1);
    // console.log('row', row);
    // this.table.deleteRow(1);
    // this.table.redraw(true)
    this.table.deleteRow(this.table.rowManager.rows[0])
  }
}
