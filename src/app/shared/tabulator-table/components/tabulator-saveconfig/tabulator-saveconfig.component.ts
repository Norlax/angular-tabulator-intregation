import { Component, OnInit, Input, ChangeDetectorRef } from '@angular/core';
import { TableComponent } from '../table/table.component';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { TabulatorService } from '../../tabulator.service';
import * as _ from 'lodash';
import { EventsManagerService } from 'src/app/shared/utils/internal-events/events-manager.service';
import { TablaConfigService } from 'src/app/shared/services/tabla-config.service';

@Component({
  selector: 'app-tabulator-saveconfig',
  templateUrl: './tabulator-saveconfig.component.html',
  styleUrls: ['./tabulator-saveconfig.component.scss']
})
export class TabulatorSaveconfigComponent implements OnInit {
  get config() {
    return TableComponent.data_config
  }
  form: FormGroup;
  @Input() tableId: any;
  @Input() seccion: any;
  @Input() config_actual: any;
  data_tabla: any;

  constructor(
    private fb: FormBuilder,
    public bsModalRef: BsModalRef,
    private event: EventsManagerService,
    private tablaConf: TablaConfigService,
    // private toast: ToastrService,
    private readonly tabulatorService: TabulatorService,
    private cd: ChangeDetectorRef
  ) { }

  ngOnInit() {
    /* console.log('configuracion', this.config_actual);
    console.log('tableID', this.tableId) */
    this.initForm();
    if (this.seccion === 'editar') {
      setTimeout(() => {
        this.setDatos();
      }, 500);
    }
    setTimeout(() => {
      // console.log('Configuracion de la tabla', this.config);
      // this.data_tabla = this.config;
      this.data_tabla = _.cloneDeep(this.config);
      /* this.data_tabla.map((i) => {
        if (Boolean(i.bottomCalc)) {
          console.log('revisa', i.bottomCalc)
          i.bottomCalc = JSON.stringify(i.bottomCalc);
        }
        if (Boolean(i.formatter)) {
          console.log('revisa', i.formatter)
          i.formatter = JSON.stringify(i.formatter);
        }
        if (Boolean(i.bottomCalcFormatter)) {
          console.log('revisa', i.bottomCalcFormatter)
          i.bottomCalcFormatter = JSON.stringify(i.bottomCalcFormatter);
        }
        return i;
      }) */
    }, 1000)
  }

  initForm() {
    this.form = this.fb.group({
      nombre_config: [null, [Validators.required]],
      tipo: ['NO']
    });
  }

  setDatos() {
    this.form.get('nombre_config').setValue(this.config_actual[0].titulo);
    this.form.get('tipo').setValue(this.config_actual[0].predeterminado);
    this.cd.detectChanges();
  }

  saveConfig() {
    const datos = {
      titulo: this.form.value.nombre_config,
      tabla: this.tableId,
      obj: this.data_tabla,
      predeterminado: this.form.value.tipo
    };

    // console.log('configuracion', datos.obj.shift());

    if (this.seccion === 'agregar') {
      datos.obj.splice(0, 1);
      // console.log('que se envia', datos);
      this.tablaConf.guardarConfig(datos).subscribe(
        (res) => {},
        (e) => {
          // this.toast.warning(e.error.mensaje);
        },
        () => {
          // this.toast.success('Guardada Exitosamente');
          this.event.publish('guardar-config', true);
          this.bsModalRef.hide();
        }
      );
    } else {
      datos.obj.splice(0, 1);
      // console.log('que se envia editar', datos, this.config_actual[0].id);
      this.tablaConf.updateConfig(datos, this.config_actual[0].id).subscribe(
        (res) => {},
        (e) => {
          // this.toast.warning(e.error.mensaje);
        },
        () => {
          // this.toast.info('Configuración Actualizada');
          this.event.publish('guardar-config', true);
          this.bsModalRef.hide();
        }
      );
    }
  }

  clear() {
    this.form.reset();
    this.bsModalRef.hide();
  }

}
