import { Component, OnInit, Input } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { TablaConfigService } from 'src/app/shared/services/tabla-config.service';
import { EventsManagerService } from 'src/app/shared/utils/internal-events/events-manager.service';

@Component({
  selector: 'app-tabulator-config-confirm',
  templateUrl: './tabulator-config-confirm.component.html',
  styleUrls: ['./tabulator-config-confirm.component.scss']
})
export class TabulatorConfigConfirmComponent implements OnInit {
  @Input() datos: any;

  constructor(
    private readonly modalService: BsModalService,
    private readonly events: EventsManagerService,
    public bsModalRef: BsModalRef,
    private tablaConf: TablaConfigService
  ) { }

  ngOnInit(): void {
  }

  deleteRegistro() {
    this.tablaConf.deleteConfig(this.datos.id).subscribe(
      (res) => {
        // this.toastrService.success('Eliminado correctamente')
      },
      (e) => {
        // this.toastrService.error(e.error.mensaje)
      },
      () => {
        this.events.publish('reload-configs', true);
        this.clear();
      }
    );
  }

  clear() {
    this.bsModalRef.hide();
  }

}
