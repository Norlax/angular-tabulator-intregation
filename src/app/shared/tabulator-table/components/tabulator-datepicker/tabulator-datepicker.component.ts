import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import * as moment from 'moment-timezone';
import {app} from "../../../../conf/app.cnf";
import {BsDaterangepickerConfig} from "ngx-bootstrap/datepicker";

@Component({
    selector: 'app-tabulator-datepicker',
    templateUrl: './tabulator-datepicker.component.html',
    styles: [`input {
        font-size: 12px;
    }`]
})
export class TabulatorDatepickerComponent implements OnInit {
    control: FormControl;
    bsConfig: Partial<BsDaterangepickerConfig> = {
        rangeInputFormat: 'DD-MM-YYYY',
        minDate: app.datepicker.min,
        maxDate: app.datepicker.max
    };

    @Output()
    search = new EventEmitter();

    ngOnInit() {
        this.initForm();
    }

    private initForm() {
        const dateRange = [
            moment().subtract(8, 'days').startOf('day').toDate(),
            moment().startOf('day').toDate(),
        ];
        this.control = new FormControl(dateRange, Validators.required);
        this.emit();
    }

    emit() {
        this.search.emit(this.control.value);
    }


}
