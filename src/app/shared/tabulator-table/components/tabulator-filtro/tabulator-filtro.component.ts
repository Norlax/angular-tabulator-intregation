import {AfterViewInit, Component, Input, OnInit} from '@angular/core';
import {TableComponent} from '../table/table.component';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
    selector: 'app-tabulator-filtro',
    templateUrl: './tabulator-filtro.component.html',
    styles: []
})
export class TabulatorFiltroComponent implements OnInit, AfterViewInit {

    @Input() table: TableComponent;
    operators = [
        {title: 'Igual', value: '='},
        {title: 'Menor o igual', value: '<='},
        {title: 'Menor', value: '<'},
        {title: 'Mayor', value: '>'},
        {title: 'Mayor o igual', value: '>='},
        {title: 'Diferente', value: '!='},
        {title: 'Contiene', value: 'like'},
    ];
    form: FormGroup;
    filters = [];

    constructor(private readonly fb: FormBuilder) {
    }

    getColumns(): Tabulator.ColumnDefinition[] {
        const cols: any[] = this.table.table.getColumns(false);
        return cols
            .map((col: Tabulator.ColumnComponent) => col.getDefinition())
            .filter(i => Boolean(i.field) && i.field.length > 1);
    }

    ngOnInit() {
        this.initForm();
    }

    initForm() {
        this.form = this.fb.group({
            field: [null, Validators.required],
            op: [null, Validators.required],
            value: [null, Validators.required],
        });
    }

    ngAfterViewInit(): void {
    }

    aplicarFiltro() {
        const filter = this.form.value;
        this.filters.push({...filter});
        const filters: Tabulator.Filter[] = this.filters.map(i => ({field: i.field, type: i.op, value: i.value}));
        this.table.table.setFilter(filters);
    }

    limpiarFiltros() {
        this.table.table.clearFilter(false);
        this.filters = [];
    }

}
