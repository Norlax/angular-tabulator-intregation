export interface OperatorOptions {
    value: Tabulator.FilterType;
    title: string;
    fn?: Function;
}

export const TabulatorsOp: OperatorOptions[] = [
    {value: '=', title: 'Igual a'},
    {value: '!=', title: 'Diferente a'},
    {value: 'like', title: 'Similar a'},
    {value: 'in', title: 'Contiene a'},
    {value: '<', title: 'Menor que'},
    {value: '>', title: 'Mayor que'},
    {value: '<=', title: 'Menor o igual que'},
    {value: '>=', title: 'Mayor o igual que'},
    {value: 'regex', title: 'Expresión Regular'},
];

export const FilterTypes = [
    {title: 'fecha', key: 'date'},
    {title: 'rango de fecha', key: 'date-range-picker'},
    {title: 'texto', key: 'text'},
    {title: 'Número', key: 'number'},
    {title: 'Lista', key: 'array'},
    {title: 'Objeto', key: 'object'},
    {title: 'Check', key: 'checkbox'},
];
