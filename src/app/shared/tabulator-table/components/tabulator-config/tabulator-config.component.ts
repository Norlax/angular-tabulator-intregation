import {Component, Input} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {TableComponent} from '../table/table.component';
import {TabulatorService} from '../../tabulator.service';
import {cloneDeep} from 'lodash';
import {BsModalRef} from "ngx-bootstrap/modal";
import * as _ from 'lodash';
import { faEye, faPencilAlt, faTrash, faStickyNote, faSuperscript } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-tabulator-config',
  templateUrl: './tabulator-config.component.html',
  styleUrls: ['./tabulator-config.component.scss'],
})
export class TabulatorConfigComponent {
  public table: TableComponent;
  public tableId: string;
  faEye = faEye;
  faPencilAlt = faPencilAlt;
  faTrash = faTrash;
  faStickyNote = faStickyNote;
  faSuperscript = faSuperscript;
  columns: Tabulator.ColumnDefinition[];

  @Input() set params(v: TableComponent) {
    this.columns = cloneDeep(v.table.getColumnDefinitions());
    this.columns = _.orderBy(this.columns, ['title', 'asc']);
    this.table = v;
    this.tableId = v.tableId;
  }

  form: FormGroup;

  constructor(
    public readonly modalRef: BsModalRef,
    private readonly tabulatorService: TabulatorService
  ) {}

  guardar() {
    this.modalRef.hide();
    this.table.table.redraw();
  }

  toggleProperty(idx: number, prop: string) {
    // console.log(prop)
    this.columns[idx][prop] = this.columns[idx].hasOwnProperty(prop)
      ? !Boolean(this.columns[idx][prop])
      : true;
    this.tabulatorService.publish(this.tableId, {
      action: prop,
      field: this.columns[idx].field,
    });
  }
}
