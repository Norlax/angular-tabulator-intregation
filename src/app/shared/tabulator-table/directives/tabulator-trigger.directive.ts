import {Directive, HostListener, Input} from '@angular/core';
import {TabulatorConfigComponent} from '../components/tabulator-config/tabulator-config.component';
import {TableComponent} from '../components/table/table.component';
import {BsModalService} from "ngx-bootstrap/modal";

@Directive({
    selector: '[appTabulatorTriggerConfig]',
})
export class TabulatorTriggerDirective {
    private table: TableComponent;
    // private columns: Tabulator.ColumnDefinition[] = [];
    // private id: string;

    @Input()
    set appTabulatorTriggerConfig(configParams: TableComponent) {
        this.table = configParams;
    }

    constructor(private readonly modalService: BsModalService) {
    }

    @HostListener('click')
    trigger() {
        this.modalService.show(TabulatorConfigComponent, {
            class: 'modal-lg',
            initialState: {params: this.table}
        });
    }
}
