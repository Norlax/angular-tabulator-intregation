import {Directive, HostListener, Input} from '@angular/core';
import {TabulatorService} from '../tabulator.service';

@Directive({
    selector: '[appTabulatorDownload]'
})
export class TabulatorDownloadDirective {
    private tableId: string;

    private options: Tabulator.DownloadParams = {
        downloadType: 'xlsx',
        //@ts-ignore
        fileName: this.tableId,
    };

    @Input()
    set appTabulatorDownload(v: string) {
        this.tableId = v;
    }

    @Input()
    set fileType(v: Tabulator.DownloadParams) {
        this.options = v;
    }

    constructor(private readonly tabulatorService: TabulatorService) {
    }

    @HostListener('click')
    downloadEmit() {
      /* console.log('ID', this.tableId)
      console.log('options', this.options) */
        this.tabulatorService.triggerAction(this.tableId, 'download', this.options);
    }

}
