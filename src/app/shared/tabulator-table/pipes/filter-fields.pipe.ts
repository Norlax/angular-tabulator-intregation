import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
    name: 'filterFields'
})
export class FilterFieldsPipe implements PipeTransform {

    transform(cols: Tabulator.ColumnDefinition[]): Tabulator.ColumnDefinition[] {
        return cols.filter(value => {
            return Boolean(value.field) && value.field.length > 0 && value.field !== '#';
        });
    }

}
