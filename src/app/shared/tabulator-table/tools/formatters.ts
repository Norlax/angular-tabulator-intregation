import * as moment from 'moment-timezone';
import { formatDate, formatDateHour, formatoMoment } from 'src/app/helpers/fecha';
import { formatDateHour2 } from 'src/app/helpers/fechas';

moment.tz.add('America/Bogota|LMT BMT -05 -04|4U.g 4U.g 50 40|01232|-3sTv3.I 1eIo0 38yo3.I 2en0|90e5');

export class Formatters {

  static DatetimeFormatter(cell: Tabulator.CellComponent, params) {
      if (!Boolean(cell.getValue())) {
          return '';
      }
      const defaultParams = {
          out: 'DD/MM/YY hh:mm A',
          invalidText: 'Fecha inválida',
          tz: 'America/Bogota',
          locale: 'es'
      };
      Object.assign(defaultParams, params);
      const date = moment(cell.getValue()).tz(defaultParams.tz).locale(defaultParams.locale);
      return date.isValid() ? date.format(defaultParams.out) : defaultParams.invalidText;
  }

  static fechaHoraFormat4(cell) {
    const fecha_format = formatoMoment(cell.getValue(), 'YYYY-MM-DD hh:mm a');
    if (fecha_format === 'Invalid date') {
      return '';
    } else {
      return `<span>${fecha_format}</span>`;
    }
  }

  static fechaHoraFormatCargaOld(cell) {
    const fecha_format = formatoMoment(cell.getValue(), 'DD/MM/YYYY HH:mm');
    if (fecha_format === 'Invalid date') {
      return '';
    } else {
      return `<span>${fecha_format}</span>`;
    }
  }

  static fechaHoraFormatCreadoOld(cell) {
    const fecha_format = formatoMoment(cell.getValue(), 'DD/MM/YY HH:mm:ss');
    if (fecha_format === 'Invalid date') {
      return '';
    } else {
      return `<span>${fecha_format}</span>`;
    }
  }

  static fechaHoraFormat5(cell) {
    const fecha_format = formatoMoment(cell.getValue(), 'DD-MM-YYYY hh:mm');
    if (fecha_format === 'Invalid date') {
      return '';
    } else {
      return `<span>${fecha_format}</span>`;
    }
  }

  static horaFormat(cell) {
    let hora = moment(cell.getValue()).format('hh:mm A');
    return hora;
  }

  static fechaHoraFormat2(cell) {
      const fecha_format = formatDateHour2(cell.getValue(), 'DD-MM-YYYY h:mm a');
      return `<span>${fecha_format}</span>`;
  }

  static fechaSimpleFormat(cell) {
    const fecha_format = formatDateHour2(cell.getValue(), 'DD-MM-YYYY');
    return `<span>${fecha_format}</span>`;
  }

  static fechaSimpleFormatB(cell) {
    const fecha_format = formatDateHour2(cell.getValue(), 'YYYY-MM-DD');
    return `<span>${fecha_format}</span>`;
  }

  static fechaSimpleFormatC(cell) {
    const fecha_format = formatDate(cell.getValue(), 'YYYY-MM-DD');
    if (fecha_format === 'Invalid date') {
      return 'Sin Fecha'
    } else {
      return `<span>${fecha_format}</span>`;
    }
  }

  static kg2Tn(cell: Tabulator.CellComponent, params: {}): string | HTMLElement {
      const _ = Object.assign({precision: 2, locale: 'es'}, params);
      const value = cell.getValue();
      if (value === 0) {
          return '';
      }
      return Number(Number(value / 1000).toFixed(_.precision || 2))
          .toLocaleString(_.locale || 'es');
  }

  static countFilteredRows(cell) {
      return `Total: ${cell.getValue()}`;
  }

  static VerificadoLabel(cell) {
      const value = cell.getValue();
      if (value === 'NO') {
          return `<span class="badge badge-danger tamano" style="width: 30% !important;"> ${value}</span>`
      } else if (value === 'SI') {
          return `<span class="badge badge-info tamano"> ${value}</span>`
      } else {
          return `<span class="badge badge-dark tamano" style="width: 80% !important;"> Sin Registro</span>`
      }
  }

  static smsFirebase(cell) {
      const value = cell.getValue();
      if (value === 'NO') {
          return `<span class="badge badge-danger tamano" style="width: 10% !important;"> ${value}</span>`
      } else if (value === 'SI') {
          return `<span class="badge badge-info tamano" style="width: 10% !important;"> ${value}</span>`
      } else {
          return `<span class="badge badge-dark tamano" style="width: 80% !important;"> Sin Registro</span>`
      }
  }

  static viewModal() {
  }

  static formatPrecioFlete(cell) {
      const valor = cell.getValue();
      if (valor) {
          /* return `<span style="color: #219653;">$ ${valor} <i class="icon-arrow-up"></i></span>`; */
          return `<span style="color: #000; font-weight: 500">$ ${valor}</span>`;
      } else {
          return valor
      }
  }

  static formatLugares(cell) {
    let valor = cell.getValue();
    if (valor) {
      return valor;
    } else {
        return 'N/A'
    }
  }

  static formatEstadoHistorial(cell) {
    const estado_maquina = cell.getValue();
    switch (estado_maquina) {
        case 1:
            return `<span class="badge badge-success"
            style="background-color: #40C057 !important; position: static; width: 95px; height: 20px; left: 1071px; top: 0px; border-radius: 3px; font-weight: 400; font-size: 0.8rem !important;">
            Activo</span>`;
            break;

        case 0:
            return `<span class="badge badge-danger"
            style="background-color: #f22222 !important; position: static; width: 95px; height: 20px; left: 1071px; top: 0px; border-radius: 3px; font-weight: 400; font-size: 0.8rem !important;">
            Inactivo</span>`;
            break;

        default:
          return `<span class="badge badge-info"
            style="background-color: #8494A7 !important; position: static; width: 95px; height: 20px; left: 1071px; top: 0px; border-radius: 3px; font-weight: 400; font-size: 0.8rem !important;">
            N/A</span>`;
            break;
        break;
    }
  }

  static formatEstadoHistorialTexto(cell) {
    const estado_maquina = cell.getValue();
    switch (estado_maquina) {
        case 'Activo':
            return `<span class="badge badge-success"
            style="background-color: #40C057 !important; position: static; width: 95px; height: 20px; left: 1071px; top: 0px; border-radius: 3px; font-weight: 400; font-size: 0.8rem !important;">
            Activo</span>`;
            break;

        case 'Inactivo':
            return `<span class="badge badge-danger"
            style="background-color: #f22222 !important; position: static; width: 95px; height: 20px; left: 1071px; top: 0px; border-radius: 3px; font-weight: 400; font-size: 0.8rem !important;">
            Inactivo</span>`;
            break;

        default:
          return `<span class="badge badge-info"
            style="background-color: #8494A7 !important; position: static; width: 95px; height: 20px; left: 1071px; top: 0px; border-radius: 3px; font-weight: 400; font-size: 0.8rem !important;">
            N/A</span>`;
            break;
        break;
    }
  }

  static formatBono(cell) {
    const valor = cell.getValue();
      if (valor) {
          return valor;
      } else {
          return 'N/A'
      }
  }

  static campoFormat(cell) {
    const valor = cell.getValue();
    if (valor) {
        return `<strong>${valor}</strong>`;
    } else {
        return ''
    }
  }

  static campoBlue(cell) {
    const valor = cell.getValue();
    if (valor) {
        return `<span style="color: #1C5BFF;">${valor}</strong></span>`;
    } else {
        return ''
    }
  }

  static campoGreen(cell) {
    const valor = cell.getValue();
    if (valor) {
        return `<span style="color: #27AE60;"><strong>${valor}</strong></span>`;
    } else {
        return ''
    }
  }

  static campoRol(cell) {
    const valor = cell.getValue();
    switch (valor) {
      case 'Todos':
        return `<span class="badge bg-light-success" style="width: 50% !important; font-size: 0.8rem; font-weight: 400"> Todos</span>`;
        break;

      case 'Logistico':
        return `<span class="badge bg-light-info" style="width: 50% !important; font-size: 0.8rem; font-weight: 400"> ${valor}</span>`;
        break;

      case 'Despachador':
        return `<span class="badge bg-light-primary" style="width: 50% !important; font-size: 0.8rem; font-weight: 400"> ${valor}</span>`;
        break;
    }
  }

  static estadoFactura(cell) {
    const valor = cell.getValue();
    switch (valor) {
      case 'PENDIENTE':
        return `<span class="badge badge-info" style="/*width: 80% !important;*/ font-size: 0.8rem; font-weight: 400"> ${valor}</span>`;
        break;

      case 'PAGO':
        return `<span class="badge badge-success" style="/*width: 50% !important;*/ font-size: 0.8rem; font-weight: 400"> PAGADA</span>`;
        break;

      case 'ELIMINADO':
        return `<span class="badge badge-danger" style="/*width: 80% !important;*/ font-size: 0.8rem; font-weight: 400"> ${valor}</span>`;
        break;
    }
  }
  static estadoCrearPuerto(cell) {
      const estado_bascula = cell.getValue();
      switch (estado_bascula.toUpperCase()) {
          case 'AUTOMATICO':
              return `<span class="badge badge-success badge-ligth-yellow-custom-apex badge-estado-creacion"> ${estado_bascula}</span>`;
              break;

          case 'MANUAL':
              return `<span class="badge badge-danger badge-ligth-purple-custom-apex badge-estado-creacion"> ${estado_bascula}</span>`;
              break;

          case 'VOLUMEN':
              return `<span class="badge badge-danger badge-ligth-azul-custom-apex badge-estado-creacion"> ${estado_bascula}</span>`;
              break;

          case 'PLACAS':
              return `<span class="badge badge-danger badge-ligth-yellow-custom-apex badge-estado-creacion"> ${estado_bascula}</span>`;
              break;

          case 'POSITIVO':
              return `<span class="badge badge-danger badge-ligth-verde-custom-apex badge-estado-creacion"><i class="ft-arrow-up"></i>&nbsp; ${estado_bascula}</span>`;
              break;

          case 'NEGATIVO':
              return `<span class="badge badge-danger badge-ligth-rosa-custom-apex badge-estado-creacion"><i class="ft-arrow-down"></i>&nbsp; ${estado_bascula}</span>`;
              break;
      }
  }

  static estado(cell) {
      const estado_maquina = cell.getValue();
      switch (estado_maquina) {
          case 'ACTIVO':
              return `<span class="badge badge-success" style="/*width: 30% !important;*/ color: #FFFFFF"> ${estado_maquina}</span>`;
              break;

          case 'INACTIVO':
              return `<span class="badge badge-danger" style="/*width: 30% !important;*/ color: #FFFFFF"> ${estado_maquina}</span>`;
              break;
      }
  }

  static estadoBascula(cell) {
      const estado_bascula = cell.getValue();
      switch (estado_bascula) {
          case 'Disponible':
              return `<span class="badge badge-success" style="/*width: 30% !important;*/ color: #FFFFFF; font-size: 0.8rem;"> ${estado_bascula}</span>`;
              break;

          case 'Sin Conexión':
              return `<span class="badge badge-danger" style="/*width: 30% !important;*/ color: #FFFFFF; font-size: 0.8rem;"> ${estado_bascula}</span>`;
              break;
      }
  }

  /* static estadoCrearPuerto(cell) {
      const estado_bascula = cell.getValue();
      switch (estado_bascula) {
          case 'Automatico':
              return `<span class="badge badge-success badge-ligth-yellow-custom-apex badge-estado-creacion"> ${estado_bascula}</span>`;
              break;

          case 'Manual':
              return `<span class="badge badge-danger badge-ligth-purple-custom-apex badge-estado-creacion"> ${estado_bascula}</span>`;
              break;

          case 'Volumen':
              return `<span class="badge badge-danger badge-ligth-azul-custom-apex badge-estado-creacion"> ${estado_bascula}</span>`;
              break;

          case 'Placas':
              return `<span class="badge badge-danger badge-ligth-yellow-custom-apex badge-estado-creacion"> ${estado_bascula}</span>`;
              break;

          case 'Positivo':
              return `<span class="badge badge-danger badge-ligth-verde-custom-apex badge-estado-creacion"><i class="ft-arrow-up"></i>&nbsp; ${estado_bascula}</span>`;
              break;

          case 'Negativo':
              return `<span class="badge badge-danger badge-ligth-rosa-custom-apex badge-estado-creacion"><i class="ft-arrow-down"></i>&nbsp; ${estado_bascula}</span>`;
              break;
      }
  } */

  static estadoNum(cell) {
    const estado = cell.getValue();
    if (estado === 1) {
      return `<span class="badge badge-success" style="width: 50% !important; color: #FFFFFF; font-size: 0.8rem"> Activo</span>`;
    } else {
      return `<span class="badge badge-danger" style="width: 50% !important; color: #FFFFFF; font-size: 0.8rem"> Inactivo</span>`;
    }
  }

  static estadoResponsive(cell) {
    let estado = cell.getValue();
    let badge;
    switch(estado) {
      case 'Activo':
        badge = `<span class="badge bg-light-success mb-1 mr-2">${estado}</span>`;
        break;
    }

    return badge
  }

  static estadoTipo(cell) {
    const estado = cell.getValue();
    if (estado === 1) {
      return `<span class="badge badge-success" style="width: 50% !important; color: #FFFFFF; font-size: 0.8rem"> Origen</span>`;
    } else {
      return `<span class="badge badge-info" style="width: 50% !important; color: #FFFFFF; font-size: 0.8rem"> Destino</span>`;
    }
  }

  static estadoOrden(cell) {
    const estado = cell.getValue();
    switch (estado) {
        case 'Enturnado':
            return `<span class="badge badge-info" style="/*width: 60% !important;*/ color: #FFFFFF; font-size: 0.8rem"> ${estado}</span>`;
            break;

        case 'Completado':
            return `<span class="badge badge-success" style="/*width: 60% !important;*/ color: #FFFFFF; font-size: 0.8rem"> ${estado}</span>`;
            break;
    }
  }

  static button() {
      return `<button class="btn btn-info btn-sm">Acciones</button>`;
  }

  static botonView() {
      return `<button class="btn btn-info btn-sm"><i class="icon-eye"></button>`;
  }

  static botones() {
      return `<button type="submit" class="btn btn-raised btn-info o-btn"><i class="icon-edit"></i></button>`
  }

  static eliminar() {
      return `<button type="submit" class="btn btn-raised btn-danger o-btn"><i class="icon-close"></i></button>`
  }

  static editar() {
      return `<button class="btn btn-info text-white">Editar</button>`;
  }

  static fechaFormat(cell) {
      const fecha_format = formatDate(cell.getValue(), 'YYYY-MM-DD');
      return `<span>${fecha_format}</span>`;
  }

  static fechaHoraFormat(cell) {
      const fecha_format = formatDateHour(cell.getValue(), 'YYYY-MM-DD, h:mm');
      return `<span>${fecha_format}</span>`;
  }

  static deleteLink(cell) {
      const value = cell.getValue();
      if (value === 'ACTIVO') {
          return `<span class="badge badge-warning white tamano" style="width: 60% !important;"> Desactivar</span>`
      }
      if (value === 'INACTIVO') {
          return `<span class="badge badge-success tamano" style="width: 60% !important;"> Activar</span>`
      }
  }

  static pesoFormate(cell) {
    const peso_neto = cell.getValue();
    if (peso_neto && peso_neto !== 0) {
      return peso_neto.toFixed(2);
    } else {
      return 0
    }
  }

  static botonPeso(cell) {
    const options = Object.assign({presicion: 2});
    let pesoFormat = cell.getValue();
    // console.log('parametro', pesoFormat);

    if (pesoFormat > 0) {
      let resultado: any = pesoFormat / 1000;
      resultado = resultado.toFixed(0);
      resultado = parseFloat(resultado);
      // console.log('resultado', Number(pesoFormat) === 0 ? 0 : Number((Number(pesoFormat) / 1000).toFixed(options.presicion)))

      const parts = resultado.toString().split(".");
      parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ".");
      const respuesta = parts.join();

      // console.log('Respuesta', respuesta)

      return `${respuesta} Tn`
    } else {
      return 0;
    }
  }

  static pesoReformat(cell) {
    let pesoFormat = cell.getValue();

    if (pesoFormat > 0) {
      let resultado = pesoFormat * 1000;

      const parts = resultado.toString().split(".");
      parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ".");
      const respuesta = parts.join();

      // console.log('Respuesta', respuesta)

      return `${respuesta} Tn`
    } else {
      return 0;
    }
  }

  static pesoSin(cell) {
    let pesoFormat = cell.getValue();

    if (pesoFormat > 0) {
      let resultado = pesoFormat;
      const parts = resultado.toString().split(".");
      parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ".");
      const respuesta = parts.join();

      return `${respuesta} Tn`;
    } else {
      return 0;
    }
  }

  static pesoKg(cell) {
    const options = Object.assign({presicion: 2});
    let pesoFormat = cell.getValue();

    if (pesoFormat > 0) {
      let resultado = pesoFormat;

      const parts = resultado.toString().split(".");
      parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ".");
      const respuesta = parts.join();

      return `${respuesta} Kg`
    } else {
      return 0;
    }
  }

  static precioFormat(cell) {
    const options = Object.assign({presicion: 2});
    let pesoFormat = cell.getValue();

    if (pesoFormat > 0) {
      let resultado = pesoFormat;
      let respuestaFormat;

      const parts = resultado.toString().split(".");
      parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ".");
      const respuesta = parts.join();

      /* console.log('respuesta', respuesta);
      respuestaFormat = parseFloat(respuesta);
      console.log('respuesta', respuestaFormat); */

      return `${respuesta} COP`
    } else {
      return 0;
    }
  }

  static precioFormatMenos(cell) {
    const options = Object.assign({presicion: 2});
    let pesoFormat = Math.abs(cell.getValue());

    if (pesoFormat > 0) {
      let resultado = pesoFormat;

      const parts = resultado.toString().split(".");
      parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ".");
      const respuesta = parts.join();

      return `-${respuesta} COP`
    } else {
      return 0;
    }
  }
}
