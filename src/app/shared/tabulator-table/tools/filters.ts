import * as moment from 'moment-timezone';

moment.tz.add('America/Bogota|LMT BMT -05 -04|4U.g 4U.g 50 40|01232|-3sTv3.I 1eIo0 38yo3.I 2en0|90e5');

export class Filters {
  static fechaHoraFilterFunc(headerValue: string, rowValue: Date, rowData: any, filterparams: any): boolean {
    const params = Object.assign({
        outputFormat: 'YYYY-MM-DD h:mm a',
        tz: 'America/Bogota',
        locale: 'es'
    }, filterparams);
    return moment(rowValue)
      .tz(params.tz)
      .locale(params.locale)
      .format(params.outputFormat)
      .includes(headerValue);
    }

    static dateFuncOld(headerValue: string, rowValue: Date, rowData: any, filterparams: any): boolean {
      const params = Object.assign({format: 'DD-MM-YYYY', tz: 'America/Bogota', locale: 'es'}, filterparams);
      return moment(rowValue).tz(params.tz).locale(params.locale).format(params.format).includes(headerValue);
  }

  static dateFuncCargaOld(headerValue: string, rowValue: Date, rowData: any, filterparams: any): boolean {
    const params = Object.assign({format: 'DD/MM/YYYY HH:mm', tz: 'America/Bogota', locale: 'es'}, filterparams);
    return moment(rowValue).tz(params.tz).locale(params.locale).format(params.format).includes(headerValue);
  }

  static dateFuncCreadoOld(headerValue: string, rowValue: Date, rowData: any, filterparams: any): boolean {
    const params = Object.assign({format: 'DD/MM/YY HH:mm:ss', tz: 'America/Bogota', locale: 'es'}, filterparams);
    return moment(rowValue).tz(params.tz).locale(params.locale).format(params.format).includes(headerValue);
  }

    static dateFunc(headerValue: string, rowValue: Date, rowData: any, filterparams: any): boolean {
        const params = Object.assign({format: 'DD-MM-YYYY hh:mm a', tz: 'America/Bogota', locale: 'es'}, filterparams);
        return  moment(rowValue)
            .tz(params.tz)
            .locale(params.locale)
            .format(params.format)
            .includes(headerValue);
    }

    static headerFecha(headerValue: string, rowValue: Date, rowData: any, filterparams: any): boolean {
        const params = Object.assign({format: 'YYYY-MM-DD hh:mm a', tz: 'America/Bogota', locale: 'es'}, filterparams);
        return  moment(rowValue)
            .tz(params.tz)
            .locale(params.locale)
            .format(params.format)
            .includes(headerValue);
    }

    static dateTimeFilterFunc(headerValue: string, rowValue: Date, rowData: any, filterparams: any): boolean {
        const params = Object.assign({
            outputFormat: 'YYYY-MM-DD',
            tz: 'America/Bogota',
            locale: 'es'
        }, filterparams);
        return moment(rowValue)
            .tz(params.tz)
            .locale(params.locale)
            .format(params.outputFormat)
            .includes(headerValue);
    }
}
