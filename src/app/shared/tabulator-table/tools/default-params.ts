import ColumnDefinition = Tabulator.ColumnDefinition;
import {Calculators} from './calculators';
import {Formatters} from './formatters';

export const defaultParams: Tabulator.Options = {
    /* General*/
    // height: '512px',
    // virtualDom: true,
    // virtualDomBuffer: true,
    // placeholder: 'No hay elementos a mostrar', // TODO: Reemplazar por NgContent
    // footerElement: '', // TODO: Reemplazar por NgContent
    reactiveData: true,
    /* History*/
    // history: true,
    /* Downloads*/
    downloadConfig: {
        columnCalcs: true,
        rowGroups: true,
        columnGroups: true
    },
    // selectable: 5,
    // selectableRangeMode: 'click',
    // selectableRollingSelection: false,
    // selectablePersistence: false,
    // movableRows: true,
    // resizableRows: true,
    // /* Rows*/
    // addRowPos: 'top',
    // /* Columns */
    // autoColumns: true,
    layout: 'fitDataFill',
    // layoutColumnsOnNewData: true,
    resizableColumns: true,
    movableColumns: true,
    // tooltipsHeader: true,
    // columnVertAlign: 'middle',
    // headerFilterPlaceholder: '',
    // scrollToColumnPosition: 'left',
    // scrollToColumnIfVisible: false,
    columnCalcs: "both",
    // columnHeaderSortMulti: true,
    /* Data*/
    index: '$id',
    // data: [],
    /* Grouping */
    groupToggleElement: 'header',
    groupClosedShowCalcs: true,
    groupStartOpen: false,
    /* Pagination */
    pagination: 'local',
    paginationSize: 10,
    paginationSizeSelector: [10, 20, 50, 100],
    paginationAddRow: 'table',
    paginationButtonCount: 5,
    /* Persitence */
    // persistenceID: 'global-config',
    // persistenceMode: true,
    // persistentLayout: true,
    // persistentSort: true,
    // persistentFilter: true,
    /* Clipboard */
    // clipboard: true,
    // clipboardCopySelector: "selected",
    // clipboardCopyFormatter: "table",
    // clipboardCopyHeader: true,
    locale: 'es',
    langs: {
        'es': {
            'pagination': {
                'page_size': 'Tamaño de paginación',
                'first': '<<', // text for the first page button
                'first_title': 'Primera página', // tooltip text for the first page button
                'last': '>>',
                'last_title': 'Última página',
                'prev': '<',
                'prev_title': 'Anterior',
                'next': '>',
                'next_title': 'Siguiente',
            },
        }
    }
};

const DefaultColumnsDefinition: { [key: string]: ColumnDefinition } = {
    'handler': {
        rowHandle: true,
        formatter: 'handle',
        headerSort: false,
        headerFilter: false,
        frozen: true,
        width: 30,
        minWidth: 30,
        field: '',
        title: '',
        download: false
    },
    'rownum': {
        formatter: 'rownum',
        title: '#',
        field: '#',
        headerFilter: false,
        headerSort: false,
        hozAlign: 'center',
        width: 60,
        frozen: true,
        bottomCalc: Calculators.countFilteredRows,
        bottomCalcFormatter: Formatters.countFilteredRows,
        download: false
    },
};

export function defaultColumns(settings: Tabulator.Options, columns: Tabulator.ColumnDefinition[]): Tabulator.ColumnDefinition[] {
    columns = [DefaultColumnsDefinition.rownum, ...columns];
    if (settings.movableRows) {
        columns = [
            DefaultColumnsDefinition.handler,
            ...columns
        ];
    }
    return columns.map(value => {
        const column = {...value};
        column.headerFilter = column.hasOwnProperty('headerFilter') ? column.headerFilter : 'input';
        column.headerSort = column.hasOwnProperty('headerSort') ? column.headerSort : true;
        column.visible = column.hasOwnProperty('visible') ? column.visible : true;
        column.frozen = column.hasOwnProperty('frozen') ? column.frozen : false;
        column.headerFilterPlaceholder = column.hasOwnProperty('headerFilterPlaceholder ')
            ? column.headerFilterPlaceholder
            : '...';
        return column;
        // return { field: column.field, title: column.title, headerFilter: column.headerFilter };
    });
}
