import * as moment from 'moment-timezone';

moment.tz.add('America/Bogota|LMT BMT -05 -04|4U.g 4U.g 50 40|01232|-3sTv3.I 1eIo0 38yo3.I 2en0|90e5');

export class AccessorsDownload {

    static DatetimeAccessor(value, data, type, params) {
        if (!Boolean(value)) {
            return '';
        }
        const defaultParams = {
            in: 'YYYY-MM-DDTHH:mm:ss',
            out: 'DD-MM-YY hh:mm A',
            invalidText: 'Fecha inválida',
            tz: 'America/Bogota'
        };
        Object.assign(defaultParams, params);
        const date = moment(value, defaultParams.in).locale('es').tz(defaultParams.tz);
        return date.isValid() ? date.format(defaultParams.out) : defaultParams.invalidText;
    }

    static DatetimeAccessorNew(value, data, type, params) {
        if (!Boolean(value)) {
            return '';
        }
        const defaultParams = {
            in: 'YYYY-MM-DDTHH:mm:ss',
            out: 'DD/MM/YYYY HH:mm',
            invalidText: 'Fecha inválida',
            tz: 'America/Bogota'
        };
        Object.assign(defaultParams, params);
        const date = moment(value, defaultParams.in).locale('es').tz(defaultParams.tz);
        return date.isValid() ? date.format(defaultParams.out) : defaultParams.invalidText;
    }

    static Kg2Tn(value, data, type, params) {
        const options = Object.assign({presicion: 2}, params);
        return Number(value) === 0 ? 0 : Number((value / 1000).toFixed(options.presicion));
    }

    static numberOrNA(value) {
        return Number(value) === 0 ? 'N/A' : Number(Number(value).toFixed(2));
    }

    static dataPatio(value, data, type, params) {
      console.group('Descarga');
      console.log('value', value);
      console.log('data', data);
      console.log('type', type);
      console.log('params', params);
      console.groupEnd();

      return data.patio
    }
}
