export class Calculators {

    static calcSumTn(values: number[]) {
        let r = 0;
        values.forEach( i => r += i);
        return r;
    }

    static countFilteredRows(values: any[]) {
        return values.length;
    }

}
