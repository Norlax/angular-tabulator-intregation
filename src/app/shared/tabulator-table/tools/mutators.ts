import * as moment from 'moment-timezone';
moment.tz.add('America/Bogota|LMT BMT -05 -04|4U.g 4U.g 50 40|01232|-3sTv3.I 1eIo0 38yo3.I 2en0|90e5');

export class Mutators {
    static Datetime(value, data, type, params) {
        if (!Boolean(value) || value.length === 0) {
            return '';
        }
        const defaultParams = {
            in: 'YYYY-MM-DDTHH:mm:ss',
            out: 'DD-MM-YY HH:mm',
            invalidText: '--/--/-- --:--',
            tz: 'America/Bogota',
            locale: 'es'
        };
        Object.assign(defaultParams, params);
        const date = moment(value, defaultParams.in).tz(defaultParams.tz).locale(defaultParams.locale);
        return date.isValid() ? date.format(defaultParams.out).toLocaleUpperCase() : defaultParams.invalidText;
    }

    static Nullable(value) {
        return String(value) === 'null' ? '' : value;
    }

    static Boolean(value) {
        return Boolean(value) ? 'SI' : 'NO';
    }

    static kg2Tn(value, data, type, params) {
        const options = Object.assign({presicion: 2}, params);
        return Number(value) === 0 ? 0 : Number((Number(value) / 1000).toFixed(options.presicion));
    }

    static numberOrNull(value) {
        return Number(value);
    }
}
