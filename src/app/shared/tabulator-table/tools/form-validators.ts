import {AbstractControl, FormArray, ValidationErrors, ValidatorFn} from "@angular/forms";

export class FormValidatorsTabulator {
    static existsFieldInArray(listAccessor: string[] | string,
                              valueAccessor: string[]| string,
                              message: string = 'El valor ya existe en la lista'): ValidatorFn {
        return (group: AbstractControl): ValidationErrors | null => {
            const value = group.get(valueAccessor).value;
            if (!Boolean(value)) {
                return null;
            }
            const list = (group.get(listAccessor) as FormArray).value
                .map(c => c.field);
            return list.includes(value.field)
                ? {alreadyExistsItem: message}
                : null;
        }
    }

}
