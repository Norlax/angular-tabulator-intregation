import { ChangeDetectorRef, Component, NgZone, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { EventsManagerService } from 'src/app/shared/utils/internal-events/events-manager.service';
import { faCheckSquare, faSquare } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-custom-select',
  templateUrl: './custom-select.component.html',
  styleUrls: ['./custom-select.component.scss'],
})
export class CustomSelectComponent implements OnInit {
  public data: any;
  faCheckSquare = faCheckSquare;
  faSquare = faSquare;

  constructor(
    private readonly cd: ChangeDetectorRef,
    private readonly event: EventsManagerService,
    private readonly ngZone: NgZone
  ) {}

  ngOnInit(): void {}

  cambiarValor() {
    this.data.preli = !this.data.preli;
    this.event.publish('factura-select', this.data);
    this.cd.detectChanges();
  }
}
