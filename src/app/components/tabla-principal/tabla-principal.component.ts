import { ChangeDetectorRef, Component, ComponentFactoryResolver, Injector, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { NgbCalendar, NgbDate, NgbDateParserFormatter } from '@ng-bootstrap/ng-bootstrap';
import { BsModalService } from 'ngx-bootstrap/modal';
import { CoreComponent } from 'src/app/shared/tabulator-table/core/core.component';
import { TabulatorService } from 'src/app/shared/tabulator-table/tabulator.service';
import { Formatters } from 'src/app/shared/tabulator-table/tools/formatters';
import { EventsManagerService } from 'src/app/shared/utils/internal-events/events-manager.service';
import { CustomSelectComponent } from '../custom-select/custom-select.component';
import { ModalPrincipalComponent } from '../modal-principal/modal-principal.component';
import { faCheckSquare, faSquare, faTrash } from '@fortawesome/free-solid-svg-icons';
import * as moment from 'moment';
import * as _ from 'lodash';
import { setNombreArchivo } from 'src/app/helpers/setExcelNombre';

@Component({
  selector: 'app-tabla-principal',
  templateUrl: './tabla-principal.component.html',
  styleUrls: ['./tabla-principal.component.scss'],
})
export class TablaPrincipalComponent extends CoreComponent implements OnInit {
  @Input() height: string = '100%';

  //Formularios
  form: FormGroup;

  //Data
  datos = [];
  static dataFiltrada = [];
  arrayEditar = [];
  faCheckSquare = faCheckSquare;
  faSquare = faSquare;
  faTrash = faTrash;

  //Fechas
  fechaInicial = moment().subtract(50, 'days').format('YYYY-MM-DD');
  fechaHoySiempre = moment().format('YYYY-MM-DD');
  toDate: NgbDate | null;
  fromDate: NgbDate | null;

  //BAnderas
  flag_tabla = false;
  select_all = false;

  columns_pendientes: Tabulator.ColumnDefinition[] = [
    {
      field: '',
      title: '---',
      hozAlign: 'center',
      download: false,
      headerFilter: false,
      headerSort: false,
      width: 50,
      formatter: (cell) => this.controladorAccion(cell),
      cellClick: (e, cell) => this.controladorBoton(cell.getData()),
    },
    {
      title: 'Item',
      field: 'item',
      hozAlign: 'center',
      responsive: 1,
      headerFilter: false,
      visible: true,
    },
    {
      title: 'Nombre',
      field: 'nombre',
      hozAlign: 'center',
      responsive: 1,
      headerFilter: false,
      visible: true,
    },
    {
      title: 'F. Creado',
      field: 'fecha_creado',
      hozAlign: 'center',
      responsive: 1,
      headerFilter: false,
      visible: true,
    },
    {
      title: 'Monto',
      field: 'monto',
      hozAlign: 'center',
      responsive: 1,
      headerFilter: false,
      visible: true,
      //@ts-ignore
      formatter: Formatters.precioFormat,
      bottomCalc: 'sum',
      //@ts-ignore
      bottomCalcFormatter: Formatters.precioFormat,
    },
    {
      title: 'Producto',
      field: 'producto',
      hozAlign: 'center',
      responsive: 1,
      headerFilter: false,
      visible: true,
    },
  ];
  tableId = 'TablaPrincipalComponent';
  settigns: Tabulator.Options = {
    placeholder: 'No hay elementos para mostrar',
    persistenceID: this.tableId,
    layout: 'fitColumns',
    groupBy: '',
    movableColumns: true,
    height: this.height,
  };

  constructor(
    private fb: FormBuilder,
    private cd: ChangeDetectorRef,
    private events: EventsManagerService,
    private _resolver: ComponentFactoryResolver,
    private _injector: Injector,
    private readonly bsModalService: BsModalService,
    private calendar: NgbCalendar,
    public formatter: NgbDateParserFormatter,
    private readonly tabulatorService: TabulatorService
  ) {
    super();
    this.fromDate = calendar.getPrev(calendar.getToday(), 'd', 50);
    this.toDate = calendar.getToday();
  }

  ngOnInit(): void {
    setNombreArchivo(`Facturar_Config_Deal`, this.tableId);
    this.subscription = this.events
      .subscribe('factura-select')
      .subscribe((data) => this.controladorArray(data));
    this.loadData();
  }

  loadData() {
    this.datos = [
      {
        item: 55785,
        preli: false,
        nombre: 'Compumax',
        fecha_creado: '2021-01-01',
        monto: 54000,
        producto: 'Tabla Refrigerante',
      },
      {
        item: 12348,
        preli: false,
        nombre: 'Compunorte',
        fecha_creado: '2021-01-05',
        monto: 80000,
        producto: 'Combo Teclado y mouse',
      },
      {
        item: 78645,
        preli: false,
        nombre: 'Compucali',
        fecha_creado: '2021-01-04',
        monto: 20000,
        producto: 'HUB',
      },
      {
        item: 45634,
        preli: false,
        nombre: 'InterCable',
        fecha_creado: '2021-01-02',
        monto: 15740,
        producto: 'Mouse',
      },
      {
        item: 45645,
        preli: false,
        nombre: 'Compumax',
        fecha_creado: '2021-01-01',
        monto: 54000,
        producto: 'Tabla Refrigerante',
      },
      {
        item: 22347,
        preli: false,
        nombre: 'Compumax',
        fecha_creado: '2021-01-01',
        monto: 54000,
        producto: 'Tabla Refrigerante',
      },
      {
        item: 57544,
        preli: false,
        nombre: 'Compumax',
        fecha_creado: '2021-01-01',
        monto: 54000,
        producto: 'Tabla Refrigerante',
      },
      {
        item: 12407,
        preli: false,
        nombre: 'Compumax',
        fecha_creado: '2021-01-01',
        monto: 54000,
        producto: 'Tabla Refrigerante',
      },
      {
        item: 78025,
        preli: false,
        nombre: 'Compumax',
        fecha_creado: '2021-01-01',
        monto: 54000,
        producto: 'Tabla Refrigerante',
      },
      {
        item: 98640,
        preli: false,
        nombre: 'Compumax',
        fecha_creado: '2021-01-01',
        monto: 54000,
        producto: 'Tabla Refrigerante',
      },
    ];
    this.flag_tabla = true;
    this.cd.detectChanges();
  }

  // Botones custom
  controladorAccion(cell: any) {
    let celda = _.cloneDeep(cell.getData());
    const factory = this._resolver.resolveComponentFactory(
      CustomSelectComponent
    );
    const component = factory.create(this._injector);
    component.instance.data = celda;
    component.changeDetectorRef.detectChanges();
    return component.location.nativeElement;
  }

  // Controlador para multiselect 1 a 1
  controladorArray(value) {
    let existe = this.arrayEditar.findIndex((i) => i.item === value.item);

    if (existe === -1) {
      this.arrayEditar.push(value);

      let item = this.arrayEditar.map((row) => row.item);
      let salida = this.datos.map((row) => {
        let encontro = item.indexOf(row.item);
        if (encontro > -1) {
          row.preli = true;
        } else {
          row.preli = false;
        }

        return row;
      });
    } else {
      this.arrayEditar.splice(existe, 1);
    }

    // this.datos = salida;
    this.cd.detectChanges();
  }

  // Controlador para multiselect selec all
  controlArrayTodas(tipo) {
    switch (tipo) {
      case 'todas':
        this.tabulatorService.publish(this.tableId, {
          action: 'data',
        });
        let datosFiltrados = TablaPrincipalComponent.dataFiltrada;
        console.log('data', datosFiltrados);
        this.arrayEditar = datosFiltrados;

        let items = datosFiltrados.map((row) => row.item);
        let salida = this.datos.map((row) => {
          let encontro = items.indexOf(row.item);
          if (encontro > -1) {
            row.preli = true;
          } else {
            // row.preli = false;
            if (row.preli === true) {
              row.preli = true;
            } else {
              row.preli = false;
            }
          }

          return row;
        });

        this.datos = salida;

        this.cd.detectChanges();

        break;
      case 'retirar':
        this.select_all = false;
        this.arrayEditar = [];
        TablaPrincipalComponent.dataFiltrada = [];

        let salida_retirar = this.datos.map((x) => {
          x.preli = false;
          return x;
        });

        this.datos = salida_retirar;

        this.cd.detectChanges();
        break;
    }
  }

  controladorBoton(item) {
    // code
  }

  modalGenerar() {
    this.bsModalService.show(ModalPrincipalComponent, {
      animated: true,
      class: 'modal-xl-full',
      initialState: { datos: this.arrayEditar, flag_nuevaedit: true },
    });
  }
}
