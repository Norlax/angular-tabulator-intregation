import { Component, Input, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-modal-principal',
  templateUrl: './modal-principal.component.html',
  styleUrls: ['./modal-principal.component.scss'],
})
export class ModalPrincipalComponent implements OnInit {
  @Input() datos: any;
  data_show = [];

  constructor(public bsModalRef: BsModalRef) {}

  ngOnInit(): void {
    this.setDatos();
  }

  setDatos() {
    this.data_show = this.datos;
  }

  clear() {
    this.bsModalRef.hide();
  }
}
