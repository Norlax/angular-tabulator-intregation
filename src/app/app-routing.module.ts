import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TablaPrincipalComponent } from './components/tabla-principal/tabla-principal.component';

const routes: Routes = [
  {
    path: '',
    component: TablaPrincipalComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
